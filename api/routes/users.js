/***** IMPORTS *****/
const bcrypt = require('bcrypt')
const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')

/***** DATABASE MODEL *****/

const Model = require('../models/Users')

/***** ROUTING *****/

router
  .route('/')
  .get((req, res) => {
    res.send('users request')
  })

router
  .route('/register')
  .post(async (req, res) => {
    const {
      email,
      name, 
      password
    } = req.query

    console.log('28',req.query)
    try {
      const fetchedUser = await Model.findOne({ email: email })
      console.log('31',fetchedUser)

      let hasEmail
      if (fetchedUser === null) hasEmail = false
      else if (fetchedUser.email === email) hasEmail = true

      if (!hasEmail) {
        const hash = await bcrypt.hash(password, 10)
        const data = await new Model({
          email: email,
          name: name,
          password: hash
        })

        data.save()

        return res.status(201).json(data)
      }
      return res.status(409).json({ message: 'email already exists' })
    } catch (error) {
      return res.status(400).json({ message: error.message })
    }
  })

router
  .route('/login')
  .get(async (req, res) => {
    const {
      email,
      password
    } = req.query

    try
    {
      const data = await Model.findOne({
        email
      })
      console.log(data)

      const isMatch = await bcrypt.compare(password, data.password)

      if (isMatch)
      {
        const jwtToken = await jwt.sign({
          exp: Math.floor(Date.now() / 1000) + (60 * 60),
          data: {
            id: data.id,
            email: email,
            name: data.name
          }
        }, 'secret')
        return res
          .cookie('token', jwtToken, { httpOnly: true })
          .status(200)
          .json({
            message: 'Login successful',
            data: {
              id: data.id,
              email: data.email,
              name: data.email
            }
          })
      }
      return res.status(404).json({ message: 'Incorrect email and/or login' })
    }
    catch (error)
    {
      return res.status(400).json({ message: error.message })
    }
  })

module.exports = router
