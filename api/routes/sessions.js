/***** IMPORTS *****/
const express = require('express')
const router = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')

/***** DATABASE MODEL *****/

const Model = require('../models/Sessions')
const Users = require('../models/Users')

/***** ROUTING *****/

router
  .route('/')
  .get(async (req, res) => {
    const {
      date,
      host_name
    } = req.query

    try {
      let data
      if (!host_name && date)
      {
        data = await Model.find({date})
      }
      else if (!data && host_name)
      {
        data = await Model.find({host_name})
      }
      else
      {
        data = await Model.find({})
      }

      if (data !== undefined || data !== null || data !== {}) 
      {
        return res.status(200).json(data)
      }

      return res.status(404).json(data)
    } catch (error) {
      return res.status(400).json({ message: error.message })
    }

  })
  .post(async (req, res) => {
    const {
      user_id,
      host_name,
      date,
      time,
      hours,
      link
    } = req.query

    const token = req.headers.cookie.substring(6)
    
    try {
      const verifyData = await Users.find({
        user_id
      })

      if (!verifyData)
      {
        return res.status(404).json({ message: 'Invalid user' })
      }

      const auth = jwt.verify(token, 'secret', function(err, decoded) {
        const current_time = Math.floor(Date.now() / 1000) + (60 * 60)
        if (decoded.exp < current_time)
        {
          return false
        }
        return true
      })

      if (!auth)
      {
        return res.status(401).json({ message: 'Unauthorized / Token Expired' })
      }

      const obj = {
        ...req.query,
        date: new Date(date),
        time: parseInt(time),
        hours: parseInt(hours),
      }

      const data = await new Model(obj)

      data.save()

      return res.status(201).json(data)
    } catch (error) {
      return res.status(400).json({ message: error.message })
    }
  })

router
  .route('/mySessions')
  .get(async (req, res) => {
    const {
      user_id
    } = req.query

    try {
    } catch (error) {
    }
  })

module.exports = router
