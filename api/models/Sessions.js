const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema({
  user_id: {
    required: true,
    type: String
  },
  host_name: {
    required: true,
    type: String
  },
  date: {
    required: true,
    type: Date
  },
  time: {
    required: false,
    type: Number
  },
  hours: {
    required: false,
    type: Number
  },
  link: {
    required: true,
    type: String
  }
})

module.exports = mongoose.model('Sessions', dataSchema)
