/* IMPORTS */
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const users = require('./routes/users')
const sessions = require('./routes/sessions')

const app = express()
require('dotenv').config()

/* DATABASE CONNECTION */

const mongoString = process.env.DATABASE_URI
mongoose.connect(mongoString);

const database = mongoose.connection

database.on('error', (error) => {
    console.log(error)
})

database.once('connected', () => {
    console.log('Database Connected');
})

/* JSON FORMATTING */

app.use(express.json())
app.use(express.urlencoded({extended: true}))

/* OTHER */

app.use(cors({
  origin: 'http://localhost:5173',
  credentials: true
}))
app.use(logger)
//app.use(express.static('public'))
//app.set('view engine', 'ejs')

/* ROUTES */

app.use('/users', users)
app.use('/sessions', sessions)
app.get('/', (req, res) => {
  res.status(200).send('default request')
})
app.use( /*default*/ (req, res) => {
  res.status(404).send('Not found!')
})

/* PORT CONNECTION */

const PORT = process.env.PORT || 8080
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`)
})

/***** FUNCTIONS *****/

function logger (req, res, next) {
  var url = req.url
  var time = new Date()
  console.log('Received request for ' + url +
  ' at ' + time)
  console.log('REQ', req.query)
  console.log('RES', res.json)
  next()
}

