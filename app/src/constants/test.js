
export const data = [
  {
    date: {
      day: 0,
      date: undefined,
      month: undefined,
      year: undefined,
    },
    time: 16,
    hours: 2,
    name: 'brian',
    course: {
      subject: 'seng',
      number: 4120,
      chapters: [1,2,3],
    },
    link: undefined
  },
  {
    date: {
      day: 1,
      date: undefined,
      month: undefined,
      year: undefined,
    },
    time: 8,
    hours: 1,
    name: 'sina',
    course: {
      subject: 'seng',
      number: 4640,
      chapters: [4, 5],
    },
    link: undefined
  },
  {
    date: {
      day: 2,
      date: undefined,
      month: undefined,
      year: undefined,
    },
    time: 8,
    hours: 1,
    name: 'geoff',
    course: {
      subject: 'ceng',
      number: 4320,
      chapters: [6, 7],
    },
    link: undefined
  },
  {
    date: {
      day: 5,
      date: undefined,
      month: undefined,
      year: undefined,
    },
    time: 17,
    hours: 4,
    name: 'adithya',
    course: {
      subject: 'seng',
      number: 4630,
      chapters: [7,8,9,10],
    },
    link: undefined
  },
  {
    date: {
      day: 0,
      date: undefined,
      month: undefined,
      year: undefined,
    },
    time: 8,
    hours: 1,
    name: 'brian',
    course: {
      subject: 'seng',
      number: 4630,
      chapters: [1,2],
    },
    link: undefined
  },
  {
    date: {
      day: 0,
      date: undefined,
      month: undefined,
      year: undefined,
    },
    time: 9,
    hours: 1,
    name: 'brian',
    course: {
      subject: 'seng',
      number: 4630,
      chapters: [1,2],
    },
    link: undefined 
  },
  {
    date: {
      day: 0,
      date: undefined,
      month: undefined,
      year: undefined,
    },
    time: 10,
    hours: 1,
    name: 'brian',
    course: {
      subject: 'seng',
      number: 4630,
      chapters: [1,2],
    },
    link: undefined 
  }
]
