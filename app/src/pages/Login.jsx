// LoginForm.js
import React, { useContext, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { UserContext } from '../context/user'
import { USER_LOGIN } from '../state/actions/index.js'

export default Login

function Login () {
  const { state, dispatch } = useContext(UserContext)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const navigate = useNavigate()

  useEffect(() => {
    if (state.is_logged_in) {
      navigate('/')
    }
  }, [])

  function handleRegister () {
    navigate('/register')
  }

  async function handleSubmit (e) {
    e.preventDefault()
    if (email !== '' || password !== '') {
      const response = await fetch(`http://localhost:8080/users/login?email=${email}&password=${password}`, {
        method: 'GET',
        credentials: 'include'
      })

      if (!response.ok) throw new Error('Failed to login')

      const { data } = await response.json()

      dispatch({
        type: USER_LOGIN,
        payload: {
          id: data.id,
          name: data.name,
          email: data.email
        }
      })
      navigate('/')
    }
    else console.log('wrong')
  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label>Email:</label>
        <input
          type='text'
        value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <div>
        <label>Password:</label>
        <input
          type='password'
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
      <button type='submit' onClick={async (e) => handleSubmit(e)}>Login</button>

      <p>Register <a onClick={handleRegister}>HERE</a></p>
    </form>
  )
}

