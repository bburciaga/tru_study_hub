import { useContext, useRef } from 'react'
import Navbar from '../components/layout/Navbar'
import AddSession from '../components/layout/AddSession'
import WeekSchedule from '../components/layout/WeekSchedule'
import { UserContext } from '../context/user'

export default Home

function Home () {
  const dialog = useRef()
  const { state, dispatch } = useContext(UserContext)

  return (
    <div>
      <Navbar />
      <AddSession dialog={dialog} />

      <div className="app">
        <WeekSchedule />
        <div style={{
          padding: '25px',
          }}>
          {state.is_logged_in && <button className="btn-primary" onClick={function () {dialog.current.showModal()}}>
            Add Session
          </button>}
        </div>
      </div>
    </div>
  )
}

