// RegisterForm.js
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'

export default Register

function Register () {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')

  const navigate = useNavigate()

  function handleLogin () {
    navigate('/login')
  }

  async function handleSubmit (e) {
    e.preventDefault()
    if (password !== confirmPassword) {
      alert('Passwords don\'t match')
      return
    }
    if (email !== '' || password !== '') {

      try {
        await fetch(
          `http://localhost:8080/users/register?email=${email}&name=${firstName}&password=${password}`,
          {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            credentials: 'include',
          }
        )
      }
      catch (error) {
        throw new Error('Failed post request')
      }

    }
    else console.log('wrong')
  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label>Email:</label>
        <input
          type='email'
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <div>
        <label>First Name:</label>
        <input
          type='text'
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
      </div>
      <div>
        <label>Last Name:</label>
        <input
          type='text'
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
      </div>
      <div>
        <label>Password:</label>
        <input
          type='password'
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
      <div>
        <label>Confirm Password:</label>
        <input
          type='password'
          value={confirmPassword}
          onChange={(e) => setConfirmPassword(e.target.value)}
        />
      </div>
      <button type='submit'>Register</button>

      <p>Already have an account? Login <a onClick={handleLogin}>HERE</a></p>
    </form>
  )
}

