// import { NavLink } from 'react-router-dom'
import { useContext, useEffect } from 'react'
import { UserContext } from './context/user'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { LOCAL_STORAGE_LOGIN } from './state/actions/index'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import './App.css'

export default function App() {
  const { state, dispatch } = useContext(UserContext)

  useEffect(() => {
    console.log(state)
    if (localStorage.user !== undefined
      && state.is_logged_in === false)
    {
      dispatch({
        type: LOCAL_STORAGE_LOGIN,
        payload: JSON.parse(localStorage.user)
      })
    }
  }, [state])

    /*  fetch("http://localhost:8080/")
    .then((res) => console.log(res))
    .then((data) => console.log(data))
    .catch((err) => console.log(err))
    */
  return (
    <BrowserRouter>

      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
      </Routes>

    </BrowserRouter>
  )
}

  /*
   
    <div>
      <Navbar />
      <AddSession dialog={dialog} />

      <div className="app">
        <WeekSchedule />
        <div style={{
          padding: '25px',
          }}>
          <button className="btn-primary" onClick={function () {dialog.current.showModal()}}>
            Add Session
          </button>
        </div>
      </div>
    </div>
        <button onClick={() => {
        fetch("http://localhost:8080/users/2", {
          method: "GET",
        })
            .then((res) => console.log(res))
            .then((data) => console.log(data))
            .catch((err) => console.log(err))
          }}>
          Login
        </button>
        */
