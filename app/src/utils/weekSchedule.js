export {
  isLeapYear,
  getDayAsString,
  getMonthAsString,
  getWeekAsArray,
  getNextSevenDays,
  organizeDays
}

function isLeapYear (year) {
  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)
}

function getDayAsString (day, short=false) {
  switch (day) {
    case 0:
      return short ? 'Sun' : 'Sunday'
    case 1:
      return short ? 'Mon' : 'Monday'
    case 2:
      return short ? 'Tue' : 'Tuesday'
    case 3:
      return short ? 'Wed' : 'Wednesday'
    case 4:
      return short ? 'Thur' : 'Thursday'
    case 5:
      return short ? 'Fri' : 'Friday'
    case 6:
      return short ? 'Sat' : 'Saturday'
  }
}

function getMonthAsString(month, short=false) {
  switch (month) {
    case 0:
      return short ? 'Jan' : 'January'
    case 1:
      return short ? 'Feb' : 'February'
    case 2:
      return short ? 'Mar' : 'March'
    case 3:
      return short ? 'Apr' : 'April'
    case 4:
      return 'May'
    case 5:
      return 'June'
    case 6:
      return 'July'
    case 7:
      return short ? 'Aug' : 'August'
    case 8:
      return short ? 'Sep' : 'September'
    case 9:
      return short ? 'Oct' : 'October'
    case 10:
      return short ? 'Nov' : 'November'
    case 11:
      return short ? 'Dec' : 'December'
  }
}

function getWeekAsArray(date) {
  const arr = [
    { day: 0, date: undefined, month: undefined },
    { day: 1, date: undefined, month: undefined },
    { day: 2, date: undefined, month: undefined },
    { day: 3, date: undefined, month: undefined },
    { day: 4, date: undefined, month: undefined },
    { day: 5, date: undefined, month: undefined },
    { day: 6, date: undefined, month: undefined },
  ]

  arr.forEach((item) => {
    let temp = date.date - (date.day - item.day)
    let max = undefined
    switch (date.month) {
      case 1:
        if (isLeapYear(date.year)) max = 29
        else max = 28
        break
      case 3:
      case 5:
      case 9:
      case 11:
        max = 30
        break
      default:
        max = 31
    }
    if (max < temp) {
      item.date = temp - max
      item.month = date.month + 1
    }
    else {
      item.date = temp
      item.month = date.month
    }
  })

  return arr;
}

function getNextSevenDays(date) {
  const arr = [
    { day: undefined, date: undefined, month: undefined, year: undefined },
    { day: undefined, date: undefined, month: undefined, year: undefined },
    { day: undefined, date: undefined, month: undefined, year: undefined },
    { day: undefined, date: undefined, month: undefined, year: undefined },
    { day: undefined, date: undefined, month: undefined, year: undefined },
    { day: undefined, date: undefined, month: undefined, year: undefined },
    { day: undefined, date: undefined, month: undefined, year: undefined },
  ]

  let day = date.getDay() + 1
  for (let i = 0; i < arr.length; i++) {
    if (day === 7) {
      day = 0
    }
    arr[i].day = day

    let max = undefined
    switch (date.getMonth()) {
      case 1:
        if (isLeapYear(date.getYear())) max = 29
        else max = 28
        break
      case 3:
      case 5:
      case 9:
      case 11:
        max = 30
        break
      default:
        max = 31
    }
    let temp = date.getDate() + i

    if (temp > max) {
      arr[i].date = temp - max
      arr[i].month = date.getMonth() + 1
    } else {
      arr[i].date = temp
      arr[i].month = date.getMonth()
    }

    arr[i].year = date.getYear()
    day = day + 1
  }

  return arr
}

function organizeDays (data) {
  let lists = [[],[],[],[],[],[],[]]
  data.forEach((item) => {
    switch (item.date.day) {
      case 0:
        lists[0].push(item)
        lists[0].sort((a,b) => {
          if (a.time < b.time) return -1
          if (a.time > b.time) return 1
          return 0
        })
        break
      case 1:
        lists[1].push(item)
        lists[1].sort((a,b) => {
          if (a.time < b.time) return -1
          if (a.time > b.time) return 1
          return 0
        })
        break
      case 2:
        lists[2].push(item)
        lists[2].sort((a,b) => {
          if (a.time < b.time) return -1
          if (a.time > b.time) return 1
          return 0
        })
        break
      case 3:
        lists[3].push(item)
        lists[3].sort((a,b) => {
          if (a.time < b.time) return -1
          if (a.time > b.time) return 1
          return 0
        })
        break
      case 4:
        lists[4].push(item)
        lists[4].sort((a,b) => {
          if (a.time < b.time) return -1
          if (a.time > b.time) return 1
          return 0
        })
        break
      case 5:
        lists[5].push(item)
        lists[5].sort((a,b) => {
          if (a.time < b.time) return -1
          if (a.time > b.time) return 1
          return 0
        })
        break
      case 6:
        lists[6].push(item)
        lists[6].sort((a,b) => {
          if (a.time < b.time) return -1
          if (a.time > b.time) return 1
          return 0
        })
        break
    }
  })

  return lists
}
