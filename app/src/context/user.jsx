import {
  createContext,
  useEffect,
  useMemo,
  useReducer
} from 'react'
import UserReducer, { initialState } from '../state/UserReducer'
import axios from 'axios'

export default UserContextProvider
export { UserContext }

const UserContext = createContext({
  state: Object,
  dispatch: dispatchEvent
})

function UserContextProvider ({children}) {
  const [state, dispatch] = useReducer(UserReducer, initialState)

  useEffect(() => {
    if (state.token)
    {
      axios.defaults.headers.common['Authorizations'] = 'Bearer ' + state.token
      localStorage.setItem('token', state.token)
    }
    else
    {
      delete axios.defaults.headers.common['Authorizations']
      localStorage.removeItem('token')
    }
  }, [state.token])

  return (
    <UserContext.Provider value={
      useMemo(() => (
           {state, dispatch}
      ), [state, dispatch])}>
      {children}
    </UserContext.Provider>
  )
}
