import {
  USER_LOGIN,
  USER_LOGOUT,
  USER_REGISTER,
  LOCAL_STORAGE_LOGIN
} from './actions/index'

export const initialState = {
  id: undefined,
  email: undefined,
  name: undefined,
  message: undefined,
  is_logged_in: false,
}

export default function UserReducer (state, action) {
  console.log(action.type, action.payload)
  switch (action.type) {
    case USER_LOGIN:
      const obj = {
        ...state,
        id: action.payload.id,
        email: action.payload.email,
        name: action.payload.name,
        message: `Welcome back ${action.payload.name}`,
        is_logged_in: true
      }

      localStorage.user = JSON.stringify(obj)
      return obj
    case USER_LOGOUT:
      return {
        ...state,
        id: undefined,
        email: undefined,
        name: undefined,
        token: null,
        message: 'Successfully Logged out',
        is_logged_in: false
      }
    case USER_REGISTER:
      return {
        ...state,
        message: 'Registration sent'
      }
    case LOCAL_STORAGE_LOGIN:
      return action.payload
    default:
      return state
  }
}
