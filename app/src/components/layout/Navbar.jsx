import { useContext, useEffect, useRef } from 'react'
import { UserContext } from '../../context/user'
import { useNavigate } from "react-router-dom";
import accountImg from '../../assets/user.png'
import loginImg from '../../assets/login.png'
import xImg from '../../assets/x-mark.png'

export default Navbar

function Navbar() {
  const { state, dispatch } = useContext(UserContext)
  const { id, name } = state
  const dialogRef = useRef()
  const navigate = useNavigate()

  function handleLogin () {
    navigate('/login')
  }

  function handleAccount () {
    fetch('http://localhost:8080/sessions/', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    })
        .then((res) => console.log(res))
        .then((data) => console.log(data))
        .catch((err) => console.log(err))
      
  }

  return (
    <nav>
      <div className='logo'>
        <h1>TRU Study Hub</h1>
      </div>
      <ul className="nav-elements">
        {state.is_logged_in && <li onClick={handleAccount}>
          <img src={accountImg} />
        </li>}
        {!state.is_logged_in && 
          <li onClick={handleLogin}>
            <img src={loginImg} />
          </li>
        }
      </ul>
      <dialog ref={dialogRef}>
        <form className='nav-dialog' method='dialog'>
          <button className='cross-btn' onClick={() => {}}>
            <img src={xImg} />
          </button>
          <div className='submitting'>
            <button className='cancel-btn'>Cancel</button>
            <button className='logout-btn'>Logout</button>
          </div>
        </form>
      </dialog>
    </nav>
  )
}
//<div className='navbar'>
    //</div>
// <a href="https://www.flaticon.com/free-icons/user" title="user icons">User icons created by Becris - Flaticon</a>
// <a href="https://www.flaticon.com/free-icons/login" title="login icons">Login icons created by mavadee - Flaticon</a>
// <a href="https://www.flaticon.com/free-icons/close" title="close icons">Close icons created by Darius Dan - Flaticon</a>
