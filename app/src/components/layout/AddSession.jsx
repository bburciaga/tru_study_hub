import { useContext, useEffect, useState } from 'react'
import { UserContext } from '../../context/user'
import {
  getDayAsString,
  getMonthAsString,
  getNextSevenDays
} from '../../utils/weekSchedule'

export default AddSession

function AddSession (props) {
  const currentDate = new Date()
  const { state, dispatch } = useContext(UserContext)
  const [selected, select] = useState({
    date: {
      day: undefined,
      date: undefined,
      month: undefined,
      year: undefined
    },
    time: 8,
    hours: 1,
    name: 'Brian',
    link: undefined
  })

  useEffect(() => {
    console.log('USESTATE', selected)
  }, [selected])

  function handleClose () {
    props.dialog.current.close()
  }

  async function handleSubmit (event) {
    console.log(selected)
    let flag = 0
    
    if (selected.date.date !== undefined
      || selected.date.month !== undefined
      || selected.date.year !== undefined
      //|| selected.link !== undefined
    ) {
      
      const isoDate = new Date(
        1900 + selected.date.year,
        selected.date.month,
        selected.date.date
      )

      try { 
        await fetch(
          `http://localhost:8080/sessions/?user_id=${state.id}&host_name=${state.name}&date=${isoDate}&time=${selected.time}&hours=${selected.hours}&link=${selected.link}`,
          {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            credentials: 'include',
          }
        )
      }
      catch (error) {
        throw new Error('Failed post request')
      }

    }

  }
  
  return (
    <dialog ref={props.dialog}>
      <form className='add-session-dialog' method='dialog'>
        <h1>New Session</h1>

        <DateSelect date={currentDate} selected={selected} select={select} />

        <TimeSelect selected={selected} select={select} />

        <LinkInput selected={selected} select={select} />

        <div>
          <button className="btn-primary" onClick={handleClose}>close</button>
          <button className="btn-primary" type="submit" onClick={handleSubmit}>Submit</button>
        </div>
      </form>
    </dialog>
  )
}

function LinkInput (props) {
  const { select, selected } = props
  const urlPattern = /^https:\/\/teams\.microsoft\.com\/l\/meetup-join\//

  function handleChange (event) {
    if (urlPattern.test(event.target.value)) {
      select({
        ...selected,
        link: event.target.value
      })
    }
  }

  return (
    <div className="session-div">
      <h4>Input link</h4>
      <div>
        <input style={{width: '300px'}} type="text" placeholder="E.g. www.google.com" onChange={handleChange} />
      </div>
    </div>
  )
}

// TODO: Refactor to prevent from value being updated before finishing of adding date
function TimeSelect (props) {
  const { select, selected } = props
  const [morning, setMorning] = useState(true)

  useEffect(() => {
    const time = +document.getElementById('time').value
    console.log(morning ? time : time + 12)
    select({
      ...selected,
      time: morning ? time : time + 12
    })
  }, [morning])
  
  function handleTime (e) {
    if (+e.target.value === 13) e.target.value = 1
    if (+e.target.value === 0) e.target.value = 12

    select({
      ...selected,
      time: morning ? +e.target.value : +e.target.value + 12
    })
  }

  function handleHours (e) {
    select({
      ...selected,
      hours: +e.target.value
    })
  }

  function handleClick (e) {
    setMorning(!morning)
  }

  return (
    <div className="session-div">
      <h4>Select a time</h4>
        <div style={{
          display: 'flex',
          flexFlow: 'row nowrap',
          alignItems: 'baseline',
          justifyContent: 'space-between'
        }}>
          <input id='time' style={{width: 50}} type='number' min='1' max='12' defaultValue='8' onChange={handleTime} />
          <p onClick={handleClick}>{morning ? 'am' : 'pm'}</p>
          <p>Hours: </p>
          <input style={{width: 50}} type='number' min='1' max='4' defaultValue='1' onChange={handleHours} />
        </div>
    </div>
  )
}

function DateSelect (props) {
  const arr = getNextSevenDays(props.date)

  return (
    <div className="session-div">
      <h4>Select a date</h4>
      <table>
        <tbody>
          <tr>
          {arr.map((item) => (
            <Day {...props} key={item.day} date={item} />
          ))}
          </tr>
        </tbody>
      </table>
    </div>
  )
}

function Day (props) {
  const {date, selected, select} = props

  function handleClick() {
    console.log(date)
    select({
      ...selected,
      date: {
        year: date.year,
        date: date.date,
        month: date.month,
        day: date.day
      }
    })
  }

  return (
    <td onClick={handleClick} style={{borderColor: date.date === selected.date.date && '#00b0b9'}}>
      <h2>{date.date}</h2>
      <br />
      <h3>{getMonthAsString(date.month, true)}</h3>
      <br />
      <h3>|</h3>
      <br />
      <h3>{getDayAsString(date.day, true)}</h3>
    </td>
  )
}

