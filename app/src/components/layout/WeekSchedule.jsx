import {
  getDayAsString,
  getMonthAsString,
  getWeekAsArray,
  organizeDays
} from '../../utils/weekSchedule'

import { data } from '../../constants/test'

export default WeekSchedule

function WeekSchedule () {
  const date = new Date()
  const currentDay = {
    day: date.getDay(),
    date: date.getDate(),
    month: date.getMonth(),
    year: date.getYear()
  }

  const arr = getWeekAsArray(currentDay)

  return (
    <div className="schedule-table">
      <table>
        <thead>
          <tr>
            <th className="time">
              Time
            </th>
            {arr.map((item) => (
              <Weekday key={item.day} object={item} />
            ))}
          </tr>
        </thead>
        <tbody>
          <DayList data={data} />
        </tbody>
      </table>
    </div>
  )
}
/*

*/

function Weekday (props) {
  console.log(window.innerWidth)
  const dayShort = window.innerWidth <= 1000 ? true : false 
  const day = getDayAsString(props.object.day, dayShort)
  const month = getMonthAsString(props.object.month, true)

  return (
    <th className="weekday">
      {day}
      <br />
      {`${month} ${props.object.date}`}
    </th>
  )
}

function DayList (props) {
  let lists = organizeDays(props.data)

  // TODO: When data has proper dates make the date and month the key
  return (
    <tr>
      <TimeColumn />
      <Cell>
        {lists[0].map((item) => (
          <Timeslot key={Math.random()} obj={item} />
        ))}
      </Cell>
      <Cell className="day-list">
        {lists[1].map((item) => (
          <Timeslot key={Math.random()} obj={item} />
        ))}
      </Cell>
      <Cell className="day-list">
        {lists[2].map((item) => (
          <Timeslot key={Math.random()} obj={item} />
        ))}
      </Cell>
      <Cell className="day-list">
        {lists[3].map((item) => (
          <Timeslot key={Math.random()} obj={item} />
        ))}
      </Cell>
      <Cell className="day-list">
        {lists[4].map((item) => (
          <Timeslot key={Math.random()} obj={item} />
        ))}
      </Cell>
      <Cell className="day-list">
        {lists[5].map((item) => (
          <Timeslot key={Math.random()} obj={item} />
        ))}
      </Cell>
      <Cell className="day-list">
        {lists[6].map((item) => (
          <Timeslot key={Math.random()} obj={item} />
        ))}
      </Cell>
    </tr>
  )
}

function Cell (props) {

  return (
    <td className="day-list">
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      <div>
        <hr />
        <hr className="dashed-line" />
      </div>
      {props.children}
    </td>
  )
}

/**
 * @param {number} time - the time when the study session starts in 24 hour time
 * @param {number} hours - the time for the study session
 * @param {string} name - the name of the admin running the session
 * @param {string} course - course subject to study 
 * @param {number[]} chapters - chapters to be reviewed in the course
*/
function Timeslot (props) {
  const {time, hours, name, course } = props.obj
  const positionTop = (time - 8) * 100

  return (
    <span className='card'
      style={{
        height: `${hours*98}px`,
        top: `${positionTop}px`,
      }}>
      {/** Admin Name **/}
      <p>{`${name}`}</p>
      {/** Course Number **/}
      <p>{`${course.subject} ${course.number}`}</p>
      {/** Chapters to cover **/}
      <p>{`Ch ${course.chapters}`}</p>
    </span>
  )
}

function TimeColumn () {
  return (
    <td className="time">
      <div>
        <p>8:00am</p>
      </div>
      <div>
        <hr />
        <p>9:00am</p>
      </div>
      <div>
        <hr />
        <p>10:00am</p>
      </div>
      <div>
        <hr />
        <p>11:00am</p>
      </div>
      <div>
        <hr />
        <p>12:00pm</p>
      </div>
      <div>
        <hr />
        <p>1:00pm</p>
      </div>
      <div>
        <hr />
        <p>2:00pm</p>
      </div>
      <div>
        <hr />
        <p>3:00pm</p>
      </div>
      <div>
        <hr />
        <p>4:00pm</p>
      </div>
      <div>
        <hr />
        <p>6:00pm</p>
      </div>
      <div>
        <hr />
        <p>8:00pm</p>
      </div>
      <div>
        <hr />
        <p>9:00pm</p>
      </div>
      <div>
        <hr />
        <p>10:00pm</p>
      </div>
    </td>
  )
}
